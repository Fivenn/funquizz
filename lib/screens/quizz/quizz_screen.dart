import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:funquizz/screens/quizz/components/progress_bar.dart';

class QuizzScreen extends StatelessWidget {
  const QuizzScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Fun Quizz',
        ),
      ),
      body: const Center(
        child: ProgressBar(),
      ),
    );
  }
}
