import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Fun Quizz',
          style: TextStyle(fontFamily: 'Quando'),
        ),
      ),
      body: const Center(
        child: Text(
          'Home Page',
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
