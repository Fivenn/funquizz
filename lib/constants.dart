import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const int timer = 30;
const double kDefaultPadding = 20;
const kPrimaryGradient = LinearGradient(
  colors: [Colors.black, Colors.blue],
  begin: Alignment.centerLeft,
  end: Alignment.centerRight,
);
