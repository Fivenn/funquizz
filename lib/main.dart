import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:funquizz/screens/quizz/quizz_screen.dart';

void main() {
  runApp(const FunQuizzApp());
}

class FunQuizzApp extends StatelessWidget {
  const FunQuizzApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fun Quizz',
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      theme: ThemeData.dark(),
      home: const QuizzScreen(),
    );
  }
}
