import 'package:flutter/animation.dart';
import 'package:funquizz/constants.dart';
import 'package:get/get.dart';

class QuestionController extends GetxController
    with SingleGetTickerProviderMixin {
  late AnimationController _animationController;
  late Animation _animation;

  Animation get animation => _animation;

  @override
  void onInit() {
    super.onInit();
    _animationController = AnimationController(
        duration: const Duration(seconds: timer), vsync: this);
    _animation = Tween<double>(begin: 0, end: 1).animate(_animationController)
      ..addListener(() {
        update();
      });
    _animationController.forward();
  }

  @override
  void onClose() {
    super.onClose();
    _animationController.dispose();
  }
}
